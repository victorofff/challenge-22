package com.luxoft.challenges.c23.parser;

import org.junit.jupiter.api.Test;

class ParserTest {

    @Test
    void vanillaParserTest() {
        Parser parser = new Parser();
        String expr = "2 * ( 1+sin( X/2 ) )^2";

        ExpressionNode node = parser.parse(expr);
        var t = 0;
    }

    @Test
    void powerTest() {
        Parser parser = new Parser();
        String expr = "4*(x+2)+3*x^2+x";

        ExpressionNode node = parser.parse(expr);

        var t = 0;
    }
}