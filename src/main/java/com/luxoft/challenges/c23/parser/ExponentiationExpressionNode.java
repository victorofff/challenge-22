package com.luxoft.challenges.c23.parser;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ExponentiationExpressionNode implements ExpressionNode {
    private final ExpressionNode base;

    private final ExpressionNode exponent;

    public int getType()
    {
        return ExpressionNode.EXPONENTIATION_NODE;
    }

    @Override
    public double getValue()
    {
        return Math.pow(base.getValue(), exponent.getValue());
    }


    @Override
    public void accept(ExpressionNodeVisitor visitor)
    {
        visitor.visit(this);
        base.accept(visitor);
        exponent.accept(visitor);
    }
}
