package com.luxoft.challenges.c23.parser;

import java.io.Serial;
import lombok.Getter;

public class ParserException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -1009747984332258423L;

    @Getter
    private Token token = null;


    public ParserException(String message) {
        super(message);
    }

    public ParserException(String message, Token token) {
        super(message);
        this.token = token;
    }


    public String getMessage() {
        String msg = super.getMessage();
        if (token != null) {
            return msg.replace("%s", token.sequence);
        }
        return msg;
    }
}
