package com.luxoft.challenges.c23.parser;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class VariableExpressionNode implements ExpressionNode {

    private final String name;

    private boolean valueSet;

    private double value;


    @Override
    public int getType() {
        return ExpressionNode.VARIABLE_NODE;
    }


    public void setValue(double value) {
        this.value = value;
        this.valueSet = true;
    }

    @Override
    public double getValue() {
        if (valueSet) {
            return value;
        } else {
            throw new EvaluationException("Variable '" + name + "' was not initialized.");
        }
    }

    @Override
    public void accept(ExpressionNodeVisitor visitor) {
        visitor.visit(this);
    }
}
