package com.luxoft.challenges.c23.parser;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

public class Tokenizer {

    @RequiredArgsConstructor
    private static class TokenInfo {

        public final Pattern regex;

        public final int token;

    }

    private final LinkedList<TokenInfo> tokenInfos = new LinkedList<>();

    @Getter
    private final LinkedList<Token> tokens = new LinkedList<>();

    private static Tokenizer expressionTokenizer = null;


    public static Tokenizer getExpressionTokenizer() {
        if (expressionTokenizer == null) {
            expressionTokenizer = createExpressionTokenizer();
        }
        return expressionTokenizer;
    }

    private static Tokenizer createExpressionTokenizer() {
        Tokenizer tokenizer = new Tokenizer();

        tokenizer.add("[+-]", Token.PLUSMINUS);
        tokenizer.add("[*/]", Token.MULTDIV);
        tokenizer.add("\\^", Token.RAISED);

        String funcs = FunctionExpressionNode.getAllFunctions();
        tokenizer.add("(" + funcs + ")(?!\\w)", Token.FUNCTION);

        tokenizer.add("\\(", Token.OPEN_BRACKET);
        tokenizer.add("\\)", Token.CLOSE_BRACKET);
        tokenizer.add("(?:\\d+\\.?|\\.\\d)\\d*(?:[Ee][-+]?\\d+)?", Token.NUMBER);
        tokenizer.add("[a-zA-Z]\\w*", Token.VARIABLE);

        return tokenizer;
    }

    public void add(String regex, int token) {
        tokenInfos.add(new TokenInfo(Pattern.compile("^(" + regex + ")"), token));
    }

    public void tokenize(String str) {
        String s = str.trim();
        int totalLength = s.length();
        tokens.clear();
        while (!s.equals("")) {
            int remaining = s.length();
            boolean match = false;
            for (TokenInfo info : tokenInfos) {
                Matcher m = info.regex.matcher(s);
                if (m.find()) {
                    match = true;
                    String tok = m.group().trim();

                    s = m.replaceFirst("").trim();
                    tokens.add(new Token(info.token, tok, totalLength - remaining));
                    break;
                }
            }
            if (!match) {
                throw new ParserException("Unexpected character in input: " + s);
            }
        }
    }

}
