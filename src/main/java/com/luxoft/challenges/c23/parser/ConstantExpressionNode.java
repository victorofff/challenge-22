package com.luxoft.challenges.c23.parser;


import lombok.Getter;

@Getter
public class ConstantExpressionNode implements ExpressionNode {

    private final double value;


    public ConstantExpressionNode(double value) {
        this.value = value;
    }

    public ConstantExpressionNode(String value) {
        this.value = Double.parseDouble(value);
    }

    @Override
    public int getType() {
        return ExpressionNode.CONSTANT_NODE;
    }

    @Override
    public void accept(ExpressionNodeVisitor visitor) {
        visitor.visit(this);
    }
}
