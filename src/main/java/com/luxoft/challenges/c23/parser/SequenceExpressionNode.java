package com.luxoft.challenges.c23.parser;

import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class SequenceExpressionNode implements ExpressionNode {

    public static class Term {

        public boolean positive;

        public ExpressionNode expression;

        public Term(boolean positive, ExpressionNode expression) {
            super();
            this.positive = positive;
            this.expression = expression;
        }
    }

    protected final List<Term> terms = new ArrayList<>();

    public SequenceExpressionNode(ExpressionNode a, boolean positive) {
        terms.add(new Term(positive, a));
    }

    public void add(ExpressionNode node, boolean positive) {
        terms.add(new Term(positive, node));
    }

}
