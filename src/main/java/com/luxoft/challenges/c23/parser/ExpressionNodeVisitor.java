package com.luxoft.challenges.c23.parser;

public interface ExpressionNodeVisitor {

    void visit(VariableExpressionNode node);

    void visit(ConstantExpressionNode node);

    void visit(AdditionExpressionNode node);

    void visit(MultiplicationExpressionNode node);

    void visit(ExponentiationExpressionNode node);

    void visit(FunctionExpressionNode node);

}