package com.luxoft.challenges.c23.parser;

import java.io.Serial;

public class EvaluationException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 4794094610927358603L;

    public EvaluationException(String message) {
        super(message);
    }
}
